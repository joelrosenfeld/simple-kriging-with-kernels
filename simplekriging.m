%Krigging Example
% This code was made to accompany the Simple Kriging episode on the YouTube
% Channel Th@MathThing http://www.thatmaththing/
% Written by Joel A. Rosenfeld in 2021. Please credit me and the video if
% you use my code for a project.

tic

% Franke Function - A standard Benchmark for function approximation
FrankeFunction = @(x) 0.75 * exp(-(9*x(1)-2)^2/4 - (9*x(2)-2)^2/4) + ...
    0.75 * exp(-(9*x(1)+1)^2/49 - (9*(2)+1)/10) + ...
    0.5 * exp(-(9*x(1)-7)^2/4 - (9*x(2)-3)^2/4) - ...
    0.2 * exp(-(9*x(1)-4)^2 - (9*x(2)-7)^2); %Franke's Function

% Kernel
mu = 1/50;
Kernel = @(x,y) exp(-1/mu*(x-y)'*(x-y)); % x and y are column vectors

% Sample Points
NumberOfSamples = 100;
x_samples = haltonseq(NumberOfSamples,2)';

Z_samples = zeros(NumberOfSamples,1);

for i = 1:NumberOfSamples
    Z_samples(i) = FrankeFunction(x_samples(:,i));
end

% Gram Matrix
GramMatrix = zeros(NumberOfSamples,NumberOfSamples);
for i=1:NumberOfSamples
    for j = 1:NumberOfSamples
        GramMatrix(i,j) = Kernel(x_samples(:,i),x_samples(:,j));
    end
end

% We are assuming that we know the mean for simple krigging
m = 0.2211;

% This depends on the sample site
CovarianceVector = @(x) exp(-1/mu*diag((x-x_samples)'*(x-x_samples))); 

% Krigging Estimator
invGram = pinv(GramMatrix);
Zhat = @(x) m+(Z_samples - m)'*invGram*CovarianceVector(x);


% Plot the Function
x = 0:0.01:1; % Row Vector of points from -1 to 1 with timestep 0.1

[X,Y] = meshgrid(x,x); % X(i,j), Y(i,j)

Z = zeros(length(x),length(x));

for i=1:length(x) % run through X axis
    for j=1:length(x) % Runs through y axis
        Z(i,j) = FrankeFunction([X(i,j);Y(i,j)]); % Evaluate the function at that domain entry
    end
end
figure
surf(X,Y,Z);

Zhatplot = zeros(length(x),length(x));

for i=1:length(x) % run through X axis
    for j=1:length(x) % Runs through y axis
        Zhatplot(i,j) = Zhat([X(i,j);Y(i,j)]); % Evaluate the function at that domain entry
    end
end
figure
surf(X,Y,Zhatplot);
toc