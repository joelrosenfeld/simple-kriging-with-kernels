%Krigging Example
% This code was made to accompany the Simple Kriging episode on the YouTube
% Channel Th@MathThing http://www.thatmaththing/
% Written by Joel A. Rosenfeld in 2021. Please credit me and the video if
% you use my code for a project.

The haltonseq.m code was obtained from MATLAB's MATHWORKS website. I can't
seem to find the original source at the moment, and this code has been
replaced in MALTAB by an inbuilt function.